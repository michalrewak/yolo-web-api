# Yolo Web APP
This is web application where you upload an image and get image with predicted bounding boxes. 
Using pretrained YOLO model by ultralytics https://ultralytics.com.

## Prerequisites
1. Ubuntu 18.04 OS
2. Installed:
   - azure-cli
   - kubectl
   - docker
### Setup Variables
   ```bash
    AKS=web-yolo-aks
    ACR=webyoloregistry001
    GROUP=wsbgroup
    LOCATION=westus
   ```
### Docker Image
Build docker image
   ```bash
   docker build -t web_app .
   ```
Test docker image
   ```bash
    docker run -it --rm  web_app:latest
   ```
### Initial Azure Setup
Login to azure
   ```bash
    az login
   ```
Create resource group
   ```bash
    az group create -l $LOCATION -n $GROUP
   ```
Debug: Check resource group
   ```bash
    az group list
   ```
Debug: Check if properly logon
   ```bash
    az account list    
   ```
### Create a container registry
   ```bash
    az acr create -n $ACR -g $GROUP --sku basic
    az acr login --name $ACR
   ```
### Push docker image to container registry
   ```bash
    docker tag web_app:latest webyoloregistry001.azurecr.io/web_app:v1
    docker push webyoloregistry001.azurecr.io/web_app:v1
   ```
## Azure Kubernetes Deploy
Create Azure Kubernetes Service with acr attached to use our docker image
   ```bash
    az aks create -n $AKS -g $GROUP --generate-ssh-keys --attach-acr $ACR
    az aks get-credentials -g $GROUP -n $AKS
   ```
Setup kubectl service
   ```bash
    kubectl apply -f web-yolo-deployment.yaml
   ```
Get public ip and paste in browser
   ```bash
    kubectl get service web-yolo-service --watch
   ```

## Azure VM Deploy(instead of Kubernetes)
## VM azure setup
Create Virtual Machine
   ```bash
    az vm create --resource-group wsbgroup --name web_yolo --size "Standard_B4ms" --image "Canonical:0001-com-ubuntu-server-focal:20_04-lts:latest"    --public-ip-sku Standard --admin-username ubuntu --ssh-key-value ~/.ssh/wsb_id_rsa.pub
   ```
Open port 80
   ```bash
    az vm open-port --resource-group wsbgroup --name web_yolo --port 80
   ```
Login to VM:
   ```bash
    ssh ubuntu@<VM_IP> -i ~/.ssh/wsb_id_rsa
   ```
Setup docker on VM:
   ```bash
    sudo apt-get update
    sudo apt-get install     ca-certificates     curl     gnupg     lsb-release
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    echo   "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
      $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt-get install docker-ce docker-ce-cli containerd.io
    sudo groupadd docker
    sudo usermod -aG docker ${USER}
   ```
Logout and run locally:
   ```bash
    TOKEN=$(az acr login --name WebYoloRegistry001 --expose-token --output tsv --query accessToken)
    ssh ubuntu@<VM_IP> docker login webyoloregistry001.azurecr.io --username 00000000-0000-0000-0000-000000000000 --password $TOKEN
   ```
Login to VM and run docker image:
   ```bash
    docker pull webyoloregistry001.azurecr.io/web_app:v1
    docker run -it --rm --net host webyoloregistry001.azurecr.io/web_app:v1
   ```







