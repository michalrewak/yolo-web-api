FROM python:3.8.0
COPY web_app web_app
RUN apt update
RUN apt install ffmpeg libsm6 libxext6  -y
RUN python -m pip install -U pip
RUN pip install -r web_app/requirements.txt
RUN pip install -e web_app
CMD ["wa-app"]
