import torch

from pathlib import Path
from flask import Flask, request, render_template, send_file

REPO = "ultralytics/yolov5"
MODEL = "yolov5s"
UPLOAD_FOLDER = "/tmp"

app = Flask(__name__)
app.config["UPLOAD_FOLDER"] = Path(UPLOAD_FOLDER)


def save_predictions_img(img, path):
    model = torch.hub.load(REPO, MODEL)
    results = model(img)
    return results.save(path)


@app.route("/", methods=["GET", "POST"])
def upload_file():
    if request.method == "POST":
        if "uploaded_file" not in request.files:
            return "Select file to upload"
        uploaded_file = request.files["uploaded_file"]
        path = app.config["UPLOAD_FOLDER"] / Path(uploaded_file.filename)
        uploaded_file.save(path)
        save_predictions_img(path, "/tmp/predictions")
        return send_file(
            Path("/tmp/predictions")
            / Path(f"{str(uploaded_file.filename).split('.')[0]}.jpg"),
            mimetype="image/gif",
        )
    return render_template("index.html")


def main():
    app.run(host="0.0.0.0", port=80)


if __name__ == "__main__":
    main()
